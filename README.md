# 보안진단 프로그램
+ 컴퓨터 사용상에 있어서 보안에 취약한 부분을 찾는 툴
+ 컴퓨터의 여러가지 취약한 부분을 찾아서 사용자에게 시각화하여 알려준다.

-------------------------

## 기능 요구사항
+ 임시 파일 제거(임시 파일 제거, 다운로드 항목 제거, 휴지통 비우기, 이전 버전의 Windows 제거)
+ 백신 업데이트 유무 확인
+ 윈도우, 바이오스 패스워드, 화면 보호기 유무 확인
+ 비밀번호 변경 유무


## 참여자 및 역할 분담
[Mutecicada](https://gitlab.com/MuteCicada)  
+ 개발자

[ysjk2003](https://gitlab.com/ysjk2003)
+ 개발자

[tjddns1975](https://gitlab.com/sungwoon)
+ PM, 개발자

[Choi_jiung](https://gitlab.com/Choi_jiung)
+ 웹 개발자

## 개발 도구
+ C
+ Window API
+ Visual studio
+ Git Lab