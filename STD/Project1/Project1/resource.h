//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// Resource.rc에서 사용되고 있습니다.
//
#define IDR_MENU2                       102
#define IDD_DIALOG6_1                   103
#define IDD_DIALOG6_2                   105
#define IDR_MENU4_1                     109
#define IDB_BITMAP1                     110
#define IDD_DIALOG6_3                   110
#define IDB_BITMAP2                     111
#define IDB_BITMAP3                     112
#define IDD_Dialog6_9                   112
#define IDB_BITMAP4                     113
#define IDB_BITMAP5                     114
#define IDD_DIALOG1                     115
#define ID_BUTTON_PRINT                 1001
#define ID_BUTTON_END                   1002
#define ID_PAUSE                        1003
#define ID_START                        1005
#define ID_CLOSE                        1006
#define ID_DELETE                       1008
#define IDC_FILELIST                    1010
#define ID_SEARCH                       1011
#define ID_40001                        40001
#define ID_40002                        40002
#define ID_40003                        40003
#define ID_40004                        40004
#define ID_40005                        40005
#define ID_40006                        40006
#define ID_40007                        40007
#define ID_40012                        40012
#define ID_40013                        40013
#define ID_40014                        40014
#define ID_40018                        40018
#define ID_Menu                         40020
#define ID_40021                        40021
#define ID_40022                        40022
#define ID_40023                        40023
#define ID_40024                        40024
#define ID_40025                        40025
#define ID_40026                        40026
#define ID_40027                        40027
#define ID_40028                        40028
#define ID_40029                        40029
#define ID_40030                        40030
#define ID_40031                        40031
#define ID_FILENEW                      40032
#define ID_FILEOPEN                     40033
#define ID_FILESAVE                     40034
#define ID_EXIT                         40035
#define ID_EDITUNDO                     40036
#define ID_EDITCOPY                     40037
#define ID_EDITPASTE                    40038
#define ID_INFORM                       40039
#define ID_40040                        40040
#define ID_6_3_MENU                     40041

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        119
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
