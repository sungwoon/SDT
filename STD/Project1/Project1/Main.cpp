#pragma once
#include <Windows.h>
#include <stdio.h>
#include <io.h>
#include <stdlib.h>
#include <string.h>
#include <tchar.h>
#include <WinUser.h>
#include <CommCtrl.h>
#include <vector>
#include <string>
#include <iostream>
#include <shlwapi.h>
#include <atlstr.h>
#include <WinUser.h>
#include "resource.h"
#pragma comment(lib, "shlwapi")
using namespace std;

HINSTANCE g_hInst;
HBITMAP hBitmap;
HBITMAP hBitmap1;
HBITMAP hBitmap2;
HBITMAP hBitmap3;
HBITMAP hBitmap4;
BITMAP bitmap;
HWND vaccine;
HWND passwd;
HWND chache;
HWND chpasswd;
HWND moniter;
PAINTSTRUCT ps;
string UserName();
vector<string> vec;
HINSTANCE hInst;
string UserProfile = UserName();
char SDT[] = "SDT";
LPSTR lpszClass = SDT;
char strPath[] = { "C:\\Program Files\\AhnLab\\V3IS90" };
string TmpPath[10] = {
	{ "C:\\Users\\" + UserProfile + "\\AppData\\Local\\Microsoft\\Windows\\INetCache\\IE" },
{ "C:\\Users\\" + UserProfile + "\\AppData\\Local\\Microsoft\\Windows\\INetCache\\LOW" },
{ "C:\\Users\\" + UserProfile + "\\AppData\\Local\\Microsoft\\Windows\\INetCookies" },
{ "C:\\Users\\" + UserProfile + "\\AppData\\Local\\Google\\Chrome\\User Data\\Default\\Cache" },
{ "C:\\Windows\\Temp" },
{ "C:\\WINDOWS\\Minidump" },
{ "C:\\Windows\\Downloaded Program Files" },
{ "C:\\Users\\" + UserProfile + "\\AppData\\Local\\Temp" },
{ "C:\\Users\\" + UserProfile + "\\AppData\\Local\\Microsoft\\Windows\\Notifications\\wpnidm" } };
int nResult = _access(strPath, 0);

string UserName()
{
	char name[20 + 1];

	DWORD dwSize = sizeof(name);
	::GetUserName(name, &dwSize);

	return name;
}

void GetFiles(string sPath)
{
	string sTmp = sPath + "\\*.*";
	WIN32_FIND_DATA fd;

	HANDLE hFind = FindFirstFile(sTmp.c_str(), &fd);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (fd.cFileName[0] != '.')
				{
					sTmp = sPath + string("\\") + string(fd.cFileName);
					GetFiles(sTmp);
				}
			}

			else
			{
				sTmp = sPath + "\\" + string(fd.cFileName);
				//	size = (fd.nFileSizeHigh * (MAXDWORD + 1)) + fd.nFileSizeLow;
				vec.push_back(sTmp);
			}

		} while (FindNextFile(hFind, &fd));

		FindClose(hFind);
	}
}

BOOL CALLBACK Dlg6_9Proc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	TCHAR fileName[MAX_PATH];
	static HWND hList;


	switch (iMsg)
	{
	case WM_INITDIALOG:
		hList = GetDlgItem(hDlg, IDC_FILELIST);
		for (int i = 0; i < 9; i++)
		{
			GetFiles(TmpPath[i]);
		}

		for (auto s : vec)
		{
			_tcscpy_s(fileName, CA2T(s.c_str()));
			SendMessage(hList, LB_ADDSTRING, 0, (LPARAM)fileName);
		}

		MessageBox(NULL, "임시파일 검색 완료", "검색", MB_OK);
		return 1;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case ID_CLOSE:
			EndDialog(hDlg, 0);
			break;

		case ID_DELETE:

			for (auto s : vec)
			{
				DeleteFile(s.c_str());
			}
			vec.clear();
			MessageBox(NULL, "삭제되었습니다", "완료", MB_OK);
			EndDialog(hDlg, 0);

		}
		break;
	}
	return 0;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	HDC HMDC;
	HDC hdc;
	LPDRAWITEMSTRUCT lpdis;
	HFONT Font;
	Font = CreateFont(50, 0, 0, 0, 0, 0, 0, 0, HANGEUL_CHARSET, 0, 0, 0, 0, "BusanBada");

	// WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON|BS_BITMAP

	switch (iMessage) {
	case WM_CREATE:
		vaccine = CreateWindow("button", "백신 업데이트", WS_CHILD | WS_VISIBLE | BS_OWNERDRAW | BS_PUSHBUTTON,
			95, 40, 128, 128, hWnd, (HMENU)0, g_hInst, NULL); //백신 업데이트 버튼 생성
		passwd = CreateWindow("button", "비밀번호 체크", WS_CHILD | WS_VISIBLE | BS_OWNERDRAW | BS_PUSHBUTTON,
			300, 40, 120, 128, hWnd, (HMENU)1, g_hInst, NULL); //비밀번호 체크 버튼 생성
		chache = CreateWindow("button", "임시파일 제거", WS_CHILD | WS_VISIBLE | BS_OWNERDRAW | BS_PUSHBUTTON,
			530, 40, 120, 128, hWnd, (HMENU)2, g_hInst, NULL); //임시파일 제거 버튼 생성
		chpasswd = CreateWindow("button", "비밀번호 변경", WS_CHILD | WS_VISIBLE | BS_OWNERDRAW | BS_PUSHBUTTON,
			730, 40, 120, 128, hWnd, (HMENU)3, g_hInst, NULL); //비밀번호 변경 유무 확인 버튼 생성
		moniter = CreateWindow("button", "화면보호기", WS_CHILD | WS_VISIBLE | BS_OWNERDRAW | BS_PUSHBUTTON,
			900, 40, 140, 126, hWnd, (HMENU)4, g_hInst, NULL); //화면보호기 버튼 생성

		hBitmap = (HBITMAP)LoadBitmap((HINSTANCE)GetWindowLong(hWnd, GWL_HINSTANCE), MAKEINTRESOURCE(IDB_BITMAP2)); // 주사기 이미지
		hBitmap1 = (HBITMAP)LoadBitmap((HINSTANCE)GetWindowLong(hWnd, GWL_HINSTANCE), MAKEINTRESOURCE(IDB_BITMAP1)); //자물쇠이미지
		hBitmap2 = (HBITMAP)LoadBitmap((HINSTANCE)GetWindowLong(hWnd, GWL_HINSTANCE), MAKEINTRESOURCE(IDB_BITMAP3)); //빗자루이미지
		hBitmap3 = (HBITMAP)LoadBitmap((HINSTANCE)GetWindowLong(hWnd, GWL_HINSTANCE), MAKEINTRESOURCE(IDB_BITMAP4)); //자물쇠 체인지 이미지
		hBitmap4 = (HBITMAP)LoadBitmap((HINSTANCE)GetWindowLong(hWnd, GWL_HINSTANCE), MAKEINTRESOURCE(IDB_BITMAP5)); //자물쇠 체인지 이미지
		
		SendMessage(vaccine, BM_SETIMAGE, (WPARAM)IMAGE_BITMAP, (LPARAM)hBitmap);
		SendMessage(passwd, BM_SETIMAGE, (WPARAM)IMAGE_BITMAP, (LPARAM)hBitmap1);
		SendMessage(chache, BM_SETIMAGE, (WPARAM)IMAGE_BITMAP, (LPARAM)hBitmap2);
		SendMessage(chpasswd, BM_SETIMAGE, (WPARAM)IMAGE_BITMAP, (LPARAM)hBitmap3);
		SendMessage(moniter, BM_SETIMAGE, (WPARAM)IMAGE_BITMAP, (LPARAM)hBitmap4);
		return 0;

	case WM_DRAWITEM:
	{
		lpdis = (LPDRAWITEMSTRUCT)lParam;
		HMDC = CreateCompatibleDC(lpdis->hDC);
		switch (lpdis->CtlID)
		{
		case 0:
			if (lpdis->itemState)
				SelectObject(HMDC, hBitmap);
			else
				SelectObject(HMDC, hBitmap);

			BitBlt(lpdis->hDC, 0, 0, 128, 128, HMDC, 0, 0, SRCCOPY);
			break;
		case 1:
			if (lpdis->itemState)
				SelectObject(HMDC, hBitmap1);
			else
				SelectObject(HMDC, hBitmap1);
			BitBlt(lpdis->hDC, 0, 0, 128, 128, HMDC, 0, 0, SRCCOPY);
			break;
		case 2:
			if (lpdis->itemState)
				SelectObject(HMDC, hBitmap2);
			else
				SelectObject(HMDC, hBitmap2);
			BitBlt(lpdis->hDC, 0, 0, 128, 128, HMDC, 0, 0, SRCCOPY);
			break;
		case 3:
			if (lpdis->itemState)
				SelectObject(HMDC, hBitmap3);
			else
				SelectObject(HMDC, hBitmap3);
			BitBlt(lpdis->hDC, 0, 0, 128, 128, HMDC, 0, 0, SRCCOPY);
			break;
		case 4:
			if (lpdis->itemState)
				SelectObject(HMDC, hBitmap4);
			else
				SelectObject(HMDC, hBitmap4);
			BitBlt(lpdis->hDC, 0, 0, 140, 128, HMDC, 0, 0, SRCCOPY);
		}
	}
	DeleteDC(HMDC);
	return 0;

	case WM_COMMAND:	//버튼 눌렸을떄 이벤트 처리
		switch (LOWORD(wParam))
		{
		case 0:
			if (nResult == 0)		//v3백신 유무 확인
			{
				BOOL bShellExecute = FALSE;
				SHELLEXECUTEINFO stShellInfo = { sizeof(SHELLEXECUTEINFO) }; // 구조체 초기화

				stShellInfo.lpVerb = TEXT("runas");                                        // 관리자 권한 실행
				stShellInfo.lpFile = TEXT("C:\\Program Files\\AhnLab\\V3IS90\\V3UpUI");     // 권리자 권한으로 실행시킬 파일
				stShellInfo.nShow = SW_SHOWNORMAL;                               // 실행될 프로세스의 윈도우 보여주기

				bShellExecute = ShellExecuteEx(&stShellInfo);
				return 0;
			}
			break;
		case 1:
			MessageBox(NULL, "asd", "ASD", MB_OK);
			//윈도우, 바이오스 비밀번호 체크 커맨드 들어갈 자리
			break;
		case 2:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_Dialog6_9), hWnd, Dlg6_9Proc);
			break;
		case 3:
			MessageBox(NULL, "check change password", "check change password", MB_OK);
			//비밀번호 변경 유무 커맨드 들어갈 자리
			break;
		case 4:
			MessageBox(NULL, "screen saver", "screensaver", MB_OK);
			//화면보호기 커맨드 들어갈 자리
			break;
		}
		return 0;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		TextOut(hdc, 110, 220, TEXT("Vaccine Update"), 14); //이미지 버튼 아래 이름
		TextOut(hdc, 300, 220, TEXT("check password"), 14);
		TextOut(hdc, 550, 220, TEXT("clear cache"), 11);
		TextOut(hdc, 720, 220, TEXT("check change password"),21);
		TextOut(hdc, 930, 220, TEXT("screen saver"), 12);
		EndPaint(hWnd, &ps);
		return 0;
	}
	return(DefWindowProc(hWnd, iMessage, wParam, lParam));
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASS WndClass;
	g_hInst = hInstance;

	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hInstance = hInstance;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.lpszClassName = lpszClass;
	WndClass.lpszMenuName = NULL;
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	RegisterClass(&WndClass);

	hWnd = CreateWindow(lpszClass, lpszClass, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
		NULL, (HMENU)NULL, hInstance, NULL);
	ShowWindow(hWnd, nCmdShow);

	while (GetMessage(&Message, 0, 0, 0)) {
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}
	return Message.wParam;
}